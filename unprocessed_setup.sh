#!/bin/bash

# This script requires NO TYPOS and that the NIFTI files in ${OUTPUT_SUBSESDIR} are already named as "${SUBJECT}_${SCAN}${NUMBER}.nii.gz", e.g. MYSUBJ_REST1.nii.gz

INPUT_SUBSESDIR=$1
OUTPUT_SUBSESDIR=$2
SUBJECT=$3          # e.g. "sub-SOMETHING"
SESSION=$4          # e.g. "ses-SOMETHING"

PIPENAME=`basename ${HCPPIPEDIR}`

### RUN THE HCP WRAPPER TO CREATE SCRIPTS ###
. submit_setup.sh ${SUBJECT} ${OUTPUT_SUBSESDIR}/${PIPENAME}

UNPROCDIR=${PROCDIR}/unprocessed/NIFTI
EPRIMEDIR=${PROCDIR}/unprocessed/EPRIME

rm -rf ${UNPROCDIR} ${EPRIMEDIR} ${PROCDIR}/REST* ${PROCDIR}/MNINonLinear/Results/REST*

#check if unprocessed and hcponeclick folders have already been created
if [ ! -d ${UNPROCDIR} ]; then
    mkdir -p ${UNPROCDIR}
fi
if [ ! -d ${EPRIMEDIR} ]; then
    mkdir -p ${EPRIMEDIR}
fi

#Symbolic-Link NIFTIs to the subject's directory if it's not already there
if [ ! `readlink -f ${INPUT_SUBSESDIR}` -ef `readlink -f ${UNPROCDIR}` ] ; then
    ln -s ${INPUT_SUBSESDIR}/nifti/*.nii.gz ${UNPROCDIR}/
fi
cp -r ${INPUT_SUBSESDIR}/eprime/* ${EPRIMEDIR}/

${OUTPUT_SUBSESDIR}/${PIPENAME}/${SUBJECT}/Scripts/hcp_runner.sh

