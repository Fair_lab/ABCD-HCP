#! /bin/bash

INPUT_SUBSESDIR=${1}
OUTPUT_SUBSESDIR=${2}

SUBJECT=`echo ${INPUT_SUBSESDIR} | rev | cut -d "/" -f2 | rev`
SESSION=`basename ${INPUT_SUBSESDIR}`

### DICOM CONVERSION TO NIFTI WITHIN THE BIDS DIRECTORY ###

if ls ${INPUT_SUBSESDIR}/*/*.nii.gz 1> /dev/null 2>&1; then
    date
    echo `date` "NIFTIs already exist. Skipping DICOM to NIFTI conversion"
else
    echo `date` "Beginning DICOM to NIFTI conversion"
    for IMGDIR in `ls -d ${INPUT_SUBSESDIR}/*/*/` ; do
        SERIES=`basename ${IMGDIR}`
        BASEDIR=`dirname ${IMGDIR}`
        echo "dcmstack -o ${SERIES} --dest-dir ${BASEDIR} ${IMGDIR}" 1>&2
        if `dcmstack -o "${SERIES}" --dest-dir ${BASEDIR} ${IMGDIR}`; then
            echo ${SERIES} Converted Succesfully
            echo ${SERIES} Converted Succesfully 1>&2
        else
            echo ${SERIES} Conversion Failed
            echo ${SERIES} Conversion Failed 1>&2
        fi
    done
    echo `date` "DICOM to NIFTI conversion complete"
fi

### CHECK IF ALL NECESSARY IMAGES EXIST ###
echo `date` "Checking for existence of required images."
i=0
if ! ls ${INPUT_SUBSESDIR}/fmap/*fMRI-FM-AP_run*.nii.gz 1> /dev/null 2>&1; then
    echo `date` "ERROR: SEFM_AP does not exist"
    i=$(( $i+1 ))
fi
if ! ls ${INPUT_SUBSESDIR}/fmap/*fMRI-FM-PA_run*.nii.gz 1> /dev/null 2>&1; then
    echo `date` "ERROR: SEFM_PA does not exist"
    i=$(( $i+=1 ))
fi
if ! ls ${INPUT_SUBSESDIR}/func/*rsfMRI_run*.nii.gz 1> /dev/null 2>&1; then
    echo `date` "ERROR: REST does not exist"
    i=$(( $i+=1 ))
fi
if ! ls ${INPUT_SUBSESDIR}/anat/*T1*_run*.nii.gz 1> /dev/null 2>&1; then
    echo `date` "ERROR: T1-NORM does not exist"
    i=$(( $i+=1 ))
fi
if ! ls ${INPUT_SUBSESDIR}/func/*-fMRI_run*.nii.gz 1> /dev/null 2>&1; then
    echo `date` "WARNING: NO TASKS exist"
fi
# If any of the necessary images are missing then automatically exit
if [ $i -gt 0 ]; then echo $i "missing images"; exit 1; else echo `date` "All required images exist."; fi


### SYMLINK ALL NIFTIs TO ../nifti/ ###

NIFTIDIR=${INPUT_SUBSESDIR}/nifti
EVENTDIR=${INPUT_SUBSESDIR}/eprime

if [ ! -d ${NIFTIDIR} ] ; then
    mkdir ${NIFTIDIR}
else
    rm -rf ${NIFTIDIR}
    mkdir ${NIFTIDIR}
fi
if [ ! -d ${EVENTDIR} ] ; then
    mkdir ${EVENTDIR}
else
    rm -rf ${EVENTDIR}
    mkdir ${EVENTDIR}
fi

echo `date` "Symlinking all NIFTIs to /nifti/ and renaming with subject id and HCP naming convention."
for IMAGE in `ls ${INPUT_SUBSESDIR}/fmap/*fMRI-FM-AP_run*.nii.gz`; do
    IMGNAME=`basename $IMAGE`
    NEWNAME=`echo $IMGNAME | sed "s|ABCD-fMRI-FM-AP_run-|${SUBJECT}_SpinEchoPhaseEncodeNegative|g"`
    ln -sf ${IMAGE} ${NIFTIDIR}/${NEWNAME}
done
for IMAGE in `ls ${INPUT_SUBSESDIR}/fmap/*fMRI-FM-PA_run*.nii.gz`; do
    IMGNAME=`basename $IMAGE`
    NEWNAME=`echo $IMGNAME | sed "s|ABCD-fMRI-FM-PA_run-|${SUBJECT}_SpinEchoPhaseEncodePositive|g"`
    ln -sf ${IMAGE} ${NIFTIDIR}/${NEWNAME}
done
for IMAGE in `ls ${INPUT_SUBSESDIR}/func/*rsfMRI_run*.nii.gz`; do
    IMGNAME=`basename $IMAGE`
    NEWNAME=`echo $IMGNAME | sed "s|ABCD-rsfMRI_run|${SUBJECT}_rfMRI_REST|g"`
    ln -sf ${IMAGE} ${NIFTIDIR}/${NEWNAME}
done
for IMAGE in `ls ${INPUT_SUBSESDIR}/func/*-fMRI_run*.nii.gz`; do
    IMGNAME=`basename $IMAGE`
    NEWNAME=`echo $IMGNAME | sed "s|ABCD-\(.*\)-fMRI_run|${SUBJECT}_tfMRI_\1|g"`
    ln -sf ${IMAGE} ${NIFTIDIR}/${NEWNAME}
    EVENTSEARCHSTR=`echo $IMAGE | sed 's|\.nii\.gz|-EventRelatedInformation.*|g'`
    ORIGEVENT=`ls -d ${EVENTSEARCHSTR}`
    NEWEVENT=`echo $NEWNAME | sed 's|\.nii\.gz|\.txt|g'`
    cp -f ${ORIGEVENT} ${EVENTDIR}/${NEWEVENT}
done

if ls ${INPUT_SUBSESDIR}/anat/*T1-NORM_run*.nii.gz 1> /dev/null 2>&1 ; then
    for IMAGE in `ls ${INPUT_SUBSESDIR}/anat/*T1-NORM_run*.nii.gz`; do
        IMGNAME=`basename $IMAGE`
        NEWNAME=`echo $IMGNAME | sed "s|ABCD-T1-NORM_run|${SUBJECT}_T1w_MPR|g"`
        ln -sf ${IMAGE} ${NIFTIDIR}/${NEWNAME}
    done
else
    for IMAGE in `ls ${INPUT_SUBSESDIR}/anat/*T1_run*.nii.gz`; do
        IMGNAME=`basename $IMAGE`
        NEWNAME=`echo $IMGNAME | sed "s|ABCD-T1_run|${SUBJECT}_T1w_MPR|g"`
        ln -sf ${IMAGE} ${NIFTIDIR}/${NEWNAME}
    done
fi

### ASSIGN PSEUDO-SERIES NUMBERS ###

echo `date` "Sorting NIFTIs by time stamp and assigning psuedo-series numbers."
pushd ${NIFTIDIR} 1> /dev/null
SORTED_NIFTI_LIST=`for NIFTI in \`ls\`; do echo ${NIFTI},${NIFTI} | sed 's|,.*_[A-Za-z]*| |g'; done | sort -k2 | sed 's| .*||g'`
sorted=( $(printf "%s " $SORTED_NIFTI_LIST) )
for (( i=0; i<${#sorted[@]}; i++  )); do
    INDEX=`echo $i+1|bc`
    IMAGESTRING=`echo ${sorted[$i]} | sed "s|201[6789].*\.nii\.gz||g"` # Will break on year 2020
    NEWNAME=`echo ${sorted[$i]} | sed "s|${IMAGESTRING}.*\.nii\.gz|${IMAGESTRING}${INDEX}\.nii\.gz|g"`
    mv ${sorted[$i]} ${NEWNAME}
    ORIGEVENT=`echo ${sorted[$i]} | sed "s|\(.*\)\.nii\.gz|\.\./eprime/\1\.txt|g"`
    NEWEVENT=`echo ${NEWNAME} | sed "s|\(.*\)\.nii\.gz|\.\./eprime/\1\.txt|g"`
    if [ -f ${ORIGEVENT} ] ; then
        mv ${ORIGEVENT} ${NEWEVENT}
    fi
done
popd 1> /dev/null
echo `date` "Pseudo-series number assignment complete"


### SELECT BEST SEFM PAIR ###

echo `date` "Selecting best SEFM pair."
sefm_eval.py -s ${SUBJECT} -d ${NIFTIDIR}
#symlink selected pair to original image
POS_SYM_LINK=`readlink ${NIFTIDIR}/${SUBJECT}_SpinEchoPhaseEncodePositive.nii.gz`
NEG_SYM_LINK=`readlink ${NIFTIDIR}/${SUBJECT}_SpinEchoPhaseEncodeNegative.nii.gz`
ln -sf `readlink ${POS_SYM_LINK}` ${NIFTIDIR}/${SUBJECT}_SpinEchoPhaseEncodePositive.nii.gz
ln -sf `readlink ${NEG_SYM_LINK}` ${NIFTIDIR}/${SUBJECT}_SpinEchoPhaseEncodeNegative.nii.gz
#remove inferior SEFM symlinks
ls ${NIFTIDIR}/* | grep -P "tive[0-9]{1,2}.nii.gz" | xargs -d"\n" rm
echo `date` "SEFM pair selection complete"

### RENAME NIFTI FILES AND EVENT RELATED INFORMATION TXT FILES IN CHRONOLOGICAL ORDER ###

echo `date` "Renaming NIFTI images in chronological order"
IMAGESTRING="_rfMRI_REST"
IMAGELIST=`ls -d ${NIFTIDIR}/*${IMAGESTRING}*`
sorted=( $(printf "%s " $IMAGELIST | sort -V) )
for (( i=0; i<${#sorted[@]}; i++ )); do
    INDEX=`echo $i+1|bc`
    NEWNAME=`echo ${sorted[$i]} | sed "s|${IMAGESTRING}.*\.nii\.gz|${IMAGESTRING}${INDEX}\.nii\.gz|g"`
    mv ${sorted[$i]} ${NEWNAME}
done
IMAGESTRING="_tfMRI_MID"
IMAGELIST=`ls -d ${NIFTIDIR}/*${IMAGESTRING}*`
sorted=( $(printf "%s " $IMAGELIST | sort -V) )
for (( i=0; i<${#sorted[@]}; i++ )); do
    INDEX=`echo $i+1|bc`
    NEWNAME=`echo ${sorted[$i]} | sed "s|${IMAGESTRING}.*\.nii\.gz|${IMAGESTRING}${INDEX}\.nii\.gz|g"`
    mv ${sorted[$i]} ${NEWNAME}
    ORIGEVENT=`echo ${sorted[$i]} | sed "s|/nifti/\(.*\)\.nii\.gz|/eprime/\1\.txt|g"`
    NEWEVENT=`echo ${NEWNAME} | sed "s|/nifti/\(.*\)\.nii\.gz|/eprime/\1\.txt|g"`
    mv ${ORIGEVENT} ${NEWEVENT}
done
IMAGESTRING="_tfMRI_nBack"
IMAGELIST=`ls -d ${NIFTIDIR}/*${IMAGESTRING}*`
sorted=( $(printf "%s " $IMAGELIST | sort -V) )
for (( i=0; i<${#sorted[@]}; i++ )); do
    INDEX=`echo $i+1|bc`
    NEWNAME=`echo ${sorted[$i]} | sed "s|${IMAGESTRING}.*\.nii\.gz|${IMAGESTRING}${INDEX}\.nii\.gz|g"`
    mv ${sorted[$i]} ${NEWNAME}
    ORIGEVENT=`echo ${sorted[$i]} | sed "s|/nifti/\(.*\)\.nii\.gz|/eprime/\1\.txt|g"`
    NEWEVENT=`echo ${NEWNAME} | sed "s|/nifti/\(.*\)\.nii\.gz|/eprime/\1\.txt|g"`
    mv ${ORIGEVENT} ${NEWEVENT}
done
IMAGESTRING="_tfMRI_SST"
IMAGELIST=`ls -d ${NIFTIDIR}/*${IMAGESTRING}*`
sorted=( $(printf "%s " $IMAGELIST | sort -V) )
for (( i=0; i<${#sorted[@]}; i++ )); do
    INDEX=`echo $i+1|bc`
    NEWNAME=`echo ${sorted[$i]} | sed "s|${IMAGESTRING}.*\.nii\.gz|${IMAGESTRING}${INDEX}\.nii\.gz|g"`
    mv ${sorted[$i]} ${NEWNAME}
    ORIGEVENT=`echo ${sorted[$i]} | sed "s|/nifti/\(.*\)\.nii\.gz|/eprime/\1\.txt|g"`
    NEWEVENT=`echo ${NEWNAME} | sed "s|/nifti/\(.*\)\.nii\.gz|/eprime/\1\.txt|g"`
    mv ${ORIGEVENT} ${NEWEVENT}
done
IMAGESTRING="_T1w_MPR"
IMAGELIST=`ls -d ${NIFTIDIR}/*${IMAGESTRING}*`
sorted=( $(printf "%s " $IMAGELIST | sort -V) )
for (( i=0; i<${#sorted[@]}; i++ )); do
    INDEX=`echo $i+1|bc`
    NEWNAME=`echo ${sorted[$i]} | sed "s|${IMAGESTRING}.*\.nii\.gz|${IMAGESTRING}${INDEX}\.nii\.gz|g"`
    mv ${sorted[$i]} ${NEWNAME}
done
echo `date` "NIFTI image renames complete"

### HCPONECLICK SETUP ###

echo `date` "Setting up hcponeclick external nifti protocol."
# Get scanner name
JSON=`ls ${INPUT_SUBSESDIR}/anat/*.json | head -n 1`
SCANNER=`jq .Manufacturer ${JSON} | awk '{print $1}'`
if [ ${SCANNER} == '"Philips' ]; then
    SCANNER="PHILIPS"
elif [ ${SCANNER} == '"GE' ]; then
    SCANNER="GE"
elif [ ${SCANNER} == '"SIEMENS"' ]; then
    SCANNER="SIEMENS"
else
    echo `date` "ERROR: Scanner not recognized"
    exit 1
fi
echo `date` "Scanner is $SCANNER"

unprocessed_setup.sh ${INPUT_SUBSESDIR} ${OUTPUT_SUBSESDIR} ${SUBJECT} ${SESSION}

echo `date` "external nifti protocol setup complete."

### PUT Scripts/hcp_runner.sh HERE ###

#### SUBMIT HCPONECLICK ###
#
#echo `date` "Submitting vol node of hcponeclick_task."
#ssh exacloud "sg fnl_lab \"condor_submit /mnt/lustre1/fnl_lab/data/HCP/processed/ABCD_${SCANNER}_SEFMNoT2/${SUBJECT}/${SESSION}/HCP_release_20161027_v1.1/hcponeclick_task/HcpVol/submit_HcpVol\""
#echo `date` "hcponeclick submission complete. Good Luck!"

