#!/usr/bin/env python3
import argparse
import os
import subprocess
from glob import glob

__version__ = open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'version')).read()

def run(command, env={}):
    merged_env = os.environ
    merged_env.update(env)
    process = subprocess.Popen(command,stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT, shell=True,
                               env=merged_env)
    while True:
        line = process.stdout.readline()
        line = str(line, 'utf-8')[:-1]
        print(line)
        if line == '' and process.poll() != None:
            break
    if process.returncode != 0:
        raise Exception("Non zero return code: %d"%process.returncode)

parser = argparse.ArgumentParser(description='ABCD-HCP Pipeline')
parser.add_argument('bids_dir', help='The directory with the input dataset '
                    'formatted according to the BIDS standard.')
parser.add_argument('output_dir', help='The directory where the output files '
                    'should be stored. If you are running group level analysis '
                    'this folder should be prepopulated with the results of the'
                    'participant level analysis.')
parser.add_argument('analysis_level', help='Level of the analysis that will be performed. '
                    'Multiple participant level analyses can be run independently '
                    '(in parallel) using the same output_dir.',
                    choices=['session','participant', 'group'])
parser.add_argument('--participant_label', help='The label(s) of the participant(s) that should be analyzed. The label '
                   'corresponds to sub-<participant_label> from the BIDS spec '
                   '(so it does not include "sub-"). If this parameter is not '
                   'provided all subjects should be analyzed. Multiple '
                   'participants can be specified with a space separated list.',
                   nargs="+")
parser.add_argument('--session_label', help='The label of the session of the participant that should be analyzed. The label '
                    ' corresponds to the session sub-directory from the BIDS spec.',
                    nargs=1)
parser.add_argument('-v', '--version', action='version',
                    version='ABCD-HCP Pipeline {}'.format(__version__))

args = parser.parse_args()

run('bids-validator %s'%args.bids_dir)

subjects_to_analyze = []
# only for a subset of subjects
if args.participant_label:
    subjects_to_analyze = args.participant_label
# for all subjects
else:
    subject_dirs = glob(os.path.join(args.bids_dir, "sub-*"))
    subjects_to_analyze = [subject_dir.split("-")[-1] for subject_dir in subject_dirs]

# running participant level
if args.analysis_level == "participant":
    # get each participant label
    for subject_label in subjects_to_analyze:
        # create subject output
        if not os.path.exists(os.path.join(args.output_dir,"sub-%s"%subject_label)):
            os.makedirs(os.path.join(args.output_dir,"sub-%s"%subject_label))
        # do for each session
        for session in glob(os.path.join(args.bids_dir, "sub-%s"%subject_label,"ses-*")):
            # create the output directory structure for this session
            outputsession = os.path.join(args.output_dir,"sub-%s"%subject_label,os.path.basename(session))
            if not os.path.exists(outputsession):
                os.makedirs(outputsession)
            # run session_prepare.sh on each session
            run("session_prepare.sh {} {}".format(session,outputsession))

# running group level
elif args.analysis_level == "group":
    # get each participant label
    for subject_label in subjects_to_analyze:
        # create subject output
        if not os.path.exists(os.path.join(args.output_dir,"sub-%s"%subject_label)):
            os.makedirs(os.path.join(args.output_dir,"sub-%s"%subject_label))
        # do for each session
        for session in glob(os.path.join(args.bids_dir, "sub-%s"%subject_label,"ses-*")):
            # create the output directory structure for this session
            outputsession = os.path.join(args.output_dir,"sub-%s"%subject_label,os.path.basename(session))
            if not os.path.exists(outputsession):
                os.makedirs(outputsession)
            # run session_prepare.sh on each session
            run("session_prepare.sh {} {}".format(session,outputsession))
