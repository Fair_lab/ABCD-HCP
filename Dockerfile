# Use Ubuntu 14.04 LTS
FROM ubuntu:trusty-20170119

## Install the validator
RUN apt-get update && \
    apt-get install -y curl && \
    curl -sL https://deb.nodesource.com/setup_6.x | bash - && \
    apt-get remove -y curl && \
    apt-get install -y nodejs && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN npm install -g bids-validator@0.19.2

# Download FreeSurfer
RUN apt-get -y update \
    && apt-get install -y wget && \
    wget -qO- ftp://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/5.3.0-HCP/freesurfer-Linux-centos4_x86_64-stable-pub-v5.3.0-HCP.tar.gz | tar zxv -C /opt \
    --exclude='freesurfer/trctrain' \
    --exclude='freesurfer/subjects/fsaverage_sym' \
    --exclude='freesurfer/subjects/fsaverage3' \
    --exclude='freesurfer/subjects/fsaverage4' \
    --exclude='freesurfer/subjects/fsaverage5' \
    --exclude='freesurfer/subjects/fsaverage6' \
    --exclude='freesurfer/subjects/cvs_avg35' \
    --exclude='freesurfer/subjects/cvs_avg35_inMNI152' \
    --exclude='freesurfer/subjects/bert' \
    --exclude='freesurfer/subjects/V1_average' \
    --exclude='freesurfer/average/mult-comp-cor' \
    --exclude='freesurfer/lib/cuda' \
    --exclude='freesurfer/lib/qt' && \
    apt-get install -y tcsh bc tar libgomp1 perl-modules curl  && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set up the environment
ENV OS Linux
ENV FS_OVERRIDE 0
ENV FIX_VERTEX_AREA=
ENV SUBJECTS_DIR /opt/freesurfer/subjects
ENV FSF_OUTPUT_FORMAT nii.gz
ENV MNI_DIR /opt/freesurfer/mni
ENV LOCAL_DIR /opt/freesurfer/local
ENV FREESURFER_HOME /opt/freesurfer
ENV FSFAST_HOME /opt/freesurfer/fsfast
ENV MINC_BIN_DIR /opt/freesurfer/mni/bin
ENV MINC_LIB_DIR /opt/freesurfer/mni/lib
ENV MNI_DATAPATH /opt/freesurfer/mni/data
ENV FMRI_ANALYSIS_DIR /opt/freesurfer/fsfast
ENV PERL5LIB /opt/freesurfer/mni/lib/perl5/5.8.5
ENV MNI_PERL5LIB /opt/freesurfer/mni/lib/perl5/5.8.5
ENV PATH /opt/freesurfer/bin:/opt/freesurfer/fsfast/bin:/opt/freesurfer/tktools:/opt/freesurfer/mni/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$PATH

# Set up FreeSurfer
RUN ${FREESURFER_HOME}/SetUpFreeSurfer.sh > /dev/null 2>&1

# Install FSL 5.0.9
RUN apt-get update && \
    apt-get install -y --no-install-recommends curl && \
    curl -sSL http://neuro.debian.net/lists/trusty.us-ca.full >> /etc/apt/sources.list.d/neurodebian.sources.list && \
    apt-key adv --recv-keys --keyserver hkp://pgp.mit.edu:80 0xA5D32F012649A5A9 && \
    apt-get update && \
    apt-get install -y fsl-core=5.0.9-4~nd14.04+1 && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Configure environment
ENV FSLDIR=/usr/share/fsl/5.0
ENV FSL_DIR="${FSLDIR}"
ENV FSLOUTPUTTYPE=NIFTI_GZ
ENV PATH=/usr/lib/fsl/5.0:$PATH
ENV FSLMULTIFILEQUIT=TRUE
ENV POSSUMDIR=/usr/share/fsl/5.0
ENV LD_LIBRARY_PATH=/usr/lib/fsl/5.0:$LD_LIBRARY_PATH
ENV FSLTCLSH=/usr/bin/tclsh
ENV FSLWISH=/usr/bin/wish
ENV FSLOUTPUTTYPE=NIFTI_GZ
RUN echo "cHJpbnRmICJrcnp5c3p0b2YuZ29yZ29sZXdza2lAZ21haWwuY29tXG41MTcyXG4gKkN2dW12RVYzelRmZ1xuRlM1Si8yYzFhZ2c0RVxuIiA+IC9vcHQvZnJlZXN1cmZlci9saWNlbnNlLnR4dAo=" | base64 -d | sh

# Setup FSL
RUN chmod +x /etc/fsl/5.0/fsl.sh
RUN ${FSLDIR}/etc/fslconf/fsl.sh

# Install Connectome Workbench
RUN apt-get update && apt-get -y install connectome-workbench=1.2.3-1~nd14.04+1

ENV CARET7DIR=/usr/bin

# Install HCP Pipelines
RUN apt-get -y update \
    && apt-get install -y --no-install-recommends python-numpy && \
    wget https://gitlab.com/Fair_lab/HCP_Pipeline/repository/docker_branch/archive.tar.gz -O pipelines.tar.gz && \
    cd /opt/ && \
    tar zxvf /pipelines.tar.gz && \
    mv /opt/HCP_Pipeline* /opt/HCP-Pipelines && \
    rm /pipelines.tar.gz && \
    cd / && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
ENV HCPPIPEDIR=/opt/HCP-Pipelines
ENV HCPPIPEDIR_Templates=${HCPPIPEDIR}/global/templates
ENV HCPPIPEDIR_Bin=${HCPPIPEDIR}/global/binaries
ENV HCPPIPEDIR_Config=${HCPPIPEDIR}/global/config
ENV HCPPIPEDIR_PreFS=${HCPPIPEDIR}/PreFreeSurfer/scripts
ENV HCPPIPEDIR_FS=${HCPPIPEDIR}/FreeSurfer/scripts
ENV HCPPIPEDIR_PostFS=${HCPPIPEDIR}/PostFreeSurfer/scripts
ENV HCPPIPEDIR_fMRISurf=${HCPPIPEDIR}/fMRISurface/scripts
ENV HCPPIPEDIR_fMRIVol=${HCPPIPEDIR}/fMRIVolume/scripts
ENV HCPPIPEDIR_tfMRI=${HCPPIPEDIR}/tfMRI/scripts
ENV HCPPIPEDIR_dMRI=${HCPPIPEDIR}/DiffusionPreprocessing/scripts
ENV HCPPIPEDIR_dMRITract=${HCPPIPEDIR}/DiffusionTractography/scripts
ENV HCPPIPEDIR_Global=${HCPPIPEDIR}/global/scripts
ENV HCPPIPEDIR_tfMRIAnalysis=${HCPPIPEDIR}/TaskfMRIAnalysis/scripts
ENV MSMBin=${HCPPIPEDIR}/MSMBinaries

# Setup the study specific protocol settings
ENV QUEUE="localhost"
ENV T1Atlas="${HCPPIPEDIR_Templates}/MNI152_T1_1mm.nii.gz"
ENV T1AtlasBrain="${HCPPIPEDIR_Templates}/MNI152_T1_1mm_brain.nii.gz"
ENV T1AtlasResamp="${HCPPIPEDIR_Templates}/MNI152_T1_2mm.nii.gz"
ENV T2Atlas="${HCPPIPEDIR_Templates}/MNI152_T2_1mm.nii.gz"
ENV T2AtlasBrain="${HCPPIPEDIR_Templates}/MNI152_T2_1mm_brain.nii.gz"
ENV T2AtlasResamp="${HCPPIPEDIR_Templates}/MNI152_T2_2mm.nii.gz"
ENV AtlasMask="${HCPPIPEDIR_Templates}/MNI152_T1_1mm_brain_mask.nii.gz"
ENV AtlasResampMask="${HCPPIPEDIR_Templates}/MNI152_T1_2mm_brain_mask_dil.nii.gz"
ENV CorrectionMethod="TOPUP"
ENV FieldmapDeltaTE="NONE"
ENV TopUpConfigFile="${HCPPIPEDIR}/global/config/b02b0.cnf"
ENV TopupUnwarpDir="y"
ENV TopupDwellTime="0.00051001152626"
ENV T1Spacing="0.0000081"
ENV T2Spacing="NONE"
ENV FOVSize="150"
ENV OutputFMRIResolution="2.0"
ENV GrayordinatesRes="2"
ENV HighResMeshKvertices="164"
ENV LowResMeshesKvertices="32"
ENV SmoothingSize="2.0"
ENV UseJacobian="false"
ENV MotionCorrectionType="MCFLIRT"
ENV BiasCorrection="NONE"
ENV useT2="false"

# Install jq
RUN apt-get update && apt-get install -y jq

# Install python and other stuff
RUN apt-get update && apt-get install -y --no-install-recommends python-pip python-six python-nibabel python-setuptools && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN pip install pybids==0.0.1
ENV PYTHONPATH=""

# Install pydicom and nibabel
RUN pip install pydicom
RUN pip install nibabel

# Download and install dcmstack
RUN wget https://github.com/moloney/dcmstack/archive/v0.6.2.zip -O dcmstack.zip && \
    easy_install dcmstack.zip && \
    rm dcmstack.zip

# Download and install Image_Pre_Evaluation
RUN wget https://gitlab.com/Fair_lab/Image_Pre_Evaluation/repository/master/archive.tar.gz -O ipe.tar.gz && \
    tar zxvf /ipe.tar.gz && \
    rm /ipe.tar.gz && \
    mv /Image_Pre_Evaluation* /opt/Image_Pre_Evaluation

# Download MCR and install
RUN apt-get update && apt-get install -y unzip
RUN mkdir /MCRdownload
WORKDIR /MCRdownload
RUN wget https://www.mathworks.com/supportfiles/downloads/R2016b/deployment_files/R2016b/installers/glnxa64/MCR_R2016b_glnxa64_installer.zip && \
    unzip MCR_R2016b_glnxa64_installer.zip && \
    ./install -mode silent -agreeToLicense yes -destinationFolder /opt/MCR
WORKDIR /
RUN rm -r /MCRdownload

# Copy ABCD-HCP files into image
COPY run.py /run.py
RUN mkdir /opt/ABCD-HCP
COPY session_prepare.sh /opt/ABCD-HCP/session_prepare.sh
COPY unprocessed_setup.sh /opt/ABCD-HCP/unprocessed_setup.sh
COPY submit_setup.sh /opt/ABCD-HCP/submit_setup.sh
RUN chmod +x /run.py

# Replace Image_Pre_Evaluation with version in ABCD-HCP folder (TEMPORARY, Update Image_Pre_Evaluation Repo) 
ADD Image_Pre_Evaluation /opt/Image_Pre_Evaluation

# Add relevant installs to PATH
#ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:/opt/MCR/v91/runtime/glnxa64:/opt/MCR/v91/bin/glnxa64:/opt/MCR/v91/sys/os/glnxa64
ENV MCR /opt/MCR
ENV PATH /opt/ABCD-HCP:/opt/Image_Pre_Evaluation:$MCR:$PATH

# Copy version file
COPY version /version

# Install Anders' favorite text editor and a simple config file for it
RUN apt-get update && apt-get install -y  vim
RUN apt-get update && apt-get install -y nano

# Makes all of opt r/w/x
#RUN chmod +rwx -R /opt

# Set entrypoint to RUN.py
ENTRYPOINT ["/run.py"]
